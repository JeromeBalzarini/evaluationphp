<?php

function getUsers()
{
  $bdd = dbConnect();
  $response = $bdd->prepare ('SELECT id_user, mail FROM user') ;
  $response->execute(array());
  return $response;
}

function getUser($idUser)
{
  $bdd = dbConnect();
  $response = $bdd->prepare ('SELECT * FROM user WHERE id_user = :idUser') ;
  $response->execute(array('idUser' => $idUser));
  return $response;
}

function createUser($mail, $pseudo, $password)
{
  $bdd = dbConnect();
  $response = $bdd -> prepare('INSERT INTO user (mail, pseudo, password) VALUES (:mail, :pseudo, :pw)');
  $response -> execute(array('mail' => $mail, 'pseudo' => $pseudo, 'pw' => $password));
  return $response;
}

function updateUser($idUser, $mail, $pseudo, $password)
{
  $bdd = dbConnect();
  $response = $bdd->prepare('UPDATE user SET mail = :mail, pseudo = :pseudo, password = :pw WHERE id_user = :id');
  $response -> execute(array('mail' => $mail, 'pseudo' => $pseudo, 'pw' => $password, 'id'=> $idUser));
  return $response;
}

function deleteUser($idUser)
{
  $bdd = dbConnect();
  $response = $bdd->prepare ('DELETE FROM `user` WHERE id_user = :idUser') ;
  $response = $response->execute(array('idUser' => $idUser));
  return $response;
}

function dbConnect(){
  try
  {
    return new PDO('mysql:host=localhost;dbname=rental;charset=utf8', 'root', '');
  }
  catch(Exception $e)
  {
    die('Erreur : '.$e->getMessage());
  }
}
 ?>
