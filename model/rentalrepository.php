<?php

function getRentals()
{
  $bdd = dbConnect();
  $response = $bdd->prepare ('SELECT r.*, u.mail FROM rental r LEFT JOIN user u ON r.id_user = u.id_user') ;
  $response->execute(array());
  return $response;
}

function getRental($idRental)
{
  $bdd = dbConnect();
  $response = $bdd->prepare ('SELECT r.*, u.mail FROM rental r LEFT JOIN user u ON r.id_user = u.id_user
                              WHERE id_rental = :idRental') ;
  $response->execute(array('idRental' => $idRental));
  return $response;
}

function createRental($title, $content, $dateBegin, $dateEnd, $idUser)
{
  $bdd = dbConnect();
  $response = $bdd->prepare('INSERT INTO rental(date_creation, title, content, date_begin, date_end, id_user)
                            VALUES (NOW(), :title, :content, :dateBegin, :dateEnd, :idUser)');
  $response->execute(array(
                          'title' => $title,
                          'content' => $content,
                          'dateBegin' => $dateBegin,
                          'dateEnd' => $dateEnd,
                          'idUser' => $idUser));
}

function updateRental($idRental, $title, $content, $dateBegin, $dateEnd, $idUser)
{
  $bdd = dbConnect();
  $response = $bdd->prepare('UPDATE rental SET
                            date_creation = NOW(),
                            title = :title,
                            content = :content,
                            date_begin = :dateBegin,
                            date_end = :dateEnd,
                            id_user = :idUser
                            WHERE id_rental = :idRental');

  $response->execute(array(
                          'title' => $title,
                          'content' => $content,
                          'dateBegin' => $dateBegin,
                          'dateEnd' => $dateEnd,
                          'idUser' => $idUser,
                          'idRental' => $idRental));
}

function deleteRental($idRental)
{
  $bdd = dbConnect();
  $response = $bdd->prepare ('DELETE FROM rental WHERE id_rental = :idRental') ;
  $response = $response->execute(array('idRental' => $idRental));
  return $response;
}

function dbConnect(){
  try
  {
    return new PDO('mysql:host=localhost;dbname=rental;charset=utf8', 'root', '');
  }
  catch(Exception $e)
  {
    die('Erreur : '.$e->getMessage());
  }
}
 ?>
