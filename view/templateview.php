<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title><?= $title ?></title>
    <link href="style.css" rel="stylesheet" />
</head>
<body>
  <header>
    -------------------------
    <br>HEADER<br>
    -------------------------
  </header>

  <?= $content ?>

  <footer>
    -------------------------
    <br>FOOTER<br>
    -------------------------
  </footer>

</body>
</html>
