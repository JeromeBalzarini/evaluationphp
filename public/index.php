<?php

echo 'Hello Worlds<br>';

$uri = $_SERVER['REQUEST_URI'];

if($result = match($uri, "/")){
  header("location:../rentals");
  die;
}

####################
### Crud Rental ####
####################
if($result = match($uri, "/rental/create")){
    require("../controller/rental/createrental.php");
    die;
}

if($result = match($uri, "/rentals")){
    require("../controller/rental/displayrentals.php");
    die;
}

if($result = match($uri, "/rental/:id")){
    require("../controller/rental/displayrental.php");
    die;
}

if($result = match($uri, "/rental/delete/:id")){
    require("../controller/rental/deleterental.php");
    die;
}

if($result = match($uri, "/rental/update/:id")){
    require("../controller/rental/updaterental.php");
    die;
}


#################
### Crud User ###
#################

if($result = match($uri, "/user/create")){
    require("../Controller/user/createUser.php");
    die;
}

if($result = match($uri, "/users")){
    require("../Controller/user/displayUsers.php");
    die;
}

if($result = match($uri, "/user/:id")){
    require("../Controller/user/displayUser.php");
    die;
}

if($result = match($uri, "/user/delete/:id")){
    require("../Controller/user/deleteUser.php");
    die;
}

if($result = match($uri, "/user/update/:id")){
    require("../Controller/user/updateUser.php");
    die;
}

echo $uri." is a wrong way !";


function match($url, $route){
  $path = preg_replace('#:([\w]+)#', '([^/]+)', $route);
  $regex = "#^$path$#i";
  if(!preg_match($regex, $url, $matches)){
      return false;
  }
  return true;
}
?>
