-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 07 mars 2018 à 12:50
-- Version du serveur :  5.7.19
-- Version de PHP :  5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `rental`
--

-- --------------------------------------------------------

--
-- Structure de la table `rental`
--

DROP TABLE IF EXISTS `rental`;
CREATE TABLE IF NOT EXISTS `rental` (
  `id_rental` int(11) NOT NULL AUTO_INCREMENT,
  `date_creation` datetime NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  `date_begin` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id_rental`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `rental`
--

INSERT INTO `rental` (`id_rental`, `date_creation`, `title`, `content`, `date_begin`, `date_end`, `id_user`) VALUES
(1, '2018-03-07 00:00:00', 'Evaluation', 'ceci est le 1er evenement de mon évaluation', '2018-03-07 13:00:00', '2018-03-07 17:30:00', 1),
(2, '2017-12-01 06:34:29', 'Nouvel an', 'BONNE ANNEE !', '2017-12-31 23:00:00', '2018-01-01 06:00:00', 2),
(3, '2018-03-05 12:20:26', 'Fake event', 'Test', '2018-03-10 08:40:33', '2018-03-11 10:30:00', 4),
(4, '2017-11-20 09:00:00', 'Début formation', 'Et c\'est partit pour la formation !', '2017-11-30 10:00:00', '2018-06-12 17:00:00', 5);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `mail` varchar(255) NOT NULL,
  `pseudo` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id_user`, `mail`, `pseudo`, `password`) VALUES
(1, '27jbalza@gmail.com', 'Jerome', 'jerome'),
(2, 'jbalzarini@free.fr', 'jbalzarini', 'jbalzarini'),
(3, 'user@user.com', 'user_test', 'user_test'),
(4, 'place@holder.com', 'place_holder', 'place_holder'),
(5, 'sujet@evaluation.fr', 'Sujet', 'sujet');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `rental`
--
ALTER TABLE `rental`
  ADD CONSTRAINT `rental_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
