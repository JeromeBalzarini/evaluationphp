<?php
require("../model/rentalrepository.php");

$response = getRentals();

if(rentalsExist($response)){
    require("../view/rental/displayrentalsview.php");
}else {
    echo 'No rental to display';
}

$response->closeCursor();

function rentalsExist($response){
  if($response->rowCount() > 0 )
  {
    return true;
  }
  return false;
}
