<?php
$errors = new ArrayObject();
$idRental = getRentalIdFromURL();

require('../model/rentalrepository.php');

$response = getRental($idRental);

if(!rentalExist($response)){
    $errors->append('Sorry there is no rental id '.$idRental);
    displayErrors($errors);
    die;
}

displayErrors($errors);

require('../view/Rental/displayRentalView.php');

function getRentalIdFromURL(){
    $url = $_SERVER['REQUEST_URI'];
    $url = explode("/", $url) ;
    $idRental = end($url) ;
    return $idRental;
}

function displayErrors($errors){
    foreach ($errors as $error)
    {
      echo $error.'<br>';
    }
}

function rentalExist($response){
    if($response->rowCount() > 0 )
    {
      return true;
    }
    return false;
}
