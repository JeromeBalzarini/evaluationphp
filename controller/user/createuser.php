<?php
$title = 'Inscription';
$errors = new ArrayObject();
require('../model/userrepository.php');

$bdd = dbConnect();

if(isFormValid($errors))
{
  $mail = $_POST['mail'];
  $pseudo = $_POST['pseudo'];
  $password = $_POST['password'];
  $password_bis = $_POST['password_bis'];
  $hash = password_hash($password);

  $bdd = dbConnect();
  $response = createUser($mail, $pseudo, $hash);

  $errors->append("Inscription réussie !");
}

ob_start();
displayErrors($errors);

require("../view/user/createuserview.php");

$content=ob_get_clean();

require("../view/templateView.php");

function displayErrors($errors)
{
  foreach ($errors as $error)
  {
    echo $error.'<br>';
  }
}

function fieldsArefilled($mail, $pseudo, $password, $password_bis)
{
  if(empty($mail) OR empty($pseudo) OR empty($password) OR empty($password_bis))
  {
    return false;
  }
  return true;
}

function variablesAreSet()
{
  if(isset($_POST['mail']) AND isset($_POST['pseudo']) AND isset($_POST['password']))
  {
    return true;
  }
  return false;
}

function isFormValid(ArrayObject $errors)
{
  if(!variablesAreSet())
  {
    $errors->append('Variables not set');
    return false;
  }

  $mail = $_POST['mail'];
  $pseudo = $_POST['pseudo'];
  $password = $_POST['password'];
  $password_bis = $_POST['password_bis'];

  if(!fieldsArefilled($mail, $pseudo, $password, $password_bis))
  {
    $errors->append('All the fields must be filled');
    return false;
  }

  if($password != $password_bis)
  {
  $errors->append('Passwords must be same');
  return false;
  }
  return true;
}

?>
