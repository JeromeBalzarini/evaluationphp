<?php
$errors = new ArrayObject();
$title="Affichages des utilisateurs";

require("../model/userrepository.php");

$response = getUsers();

if(!usersExist($response))
{
  $errors->append('Sorry there is no user');
  displayErrors($errors);
  die;
}

ob_start();
displayErrors($errors);

require('../view/user/displayusersview.php');

$content=ob_get_clean();

require('../view/templateview.php');

$response->closeCursor();

function usersExist($response){
  if($response->rowCount() > 0 )
  {
    return true;
  }
  return false;
}

function displayErrors($errors){
    foreach ($errors as $error) {
        echo $error . '<br>';
    }
}
