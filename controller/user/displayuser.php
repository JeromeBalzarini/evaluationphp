<?php
$title = 'Affichage d\'un utilisateur';
$errors = new ArrayObject();
$idUser = getUserIdFromURL();

require('../Model/userRepository.php');

$response = getUser($idUser);

if(!userExist($response)){
    $errors->append('Sorry there is no user id '.$idUser);
    displayErrors($errors);
    die;
}

ob_start();
displayErrors($errors);

require('../view/user/displayuserview.php');

$content=ob_get_clean();

require('../view/templateview.php');

$response->closeCursor();

function getUserIdFromURL(){
    $url = $_SERVER['REQUEST_URI'];
    $url = explode("/", $url) ;
    $authorId = end($url) ;
    return $authorId;
}

function displayErrors($errors){
    foreach ($errors as $error)
    {
      echo $error.'<br>';
    }
}

function userExist($response){
    if($response->rowCount() > 0 )
    {
      return true;
    }
    return false;
}
